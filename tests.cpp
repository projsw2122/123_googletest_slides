// tests.cpp
#include "code.cpp"
#include <gtest/gtest.h>

/*
// http://code.google.com/p/googletest/wiki/Primer#Assertions

TEST(SquareRootTest, PositiveNos) { 
    ASSERT_EQ(6, squareRoot(36.0));
    ASSERT_EQ(18.0, squareRoot(324.0));
    ASSERT_EQ(25.4, squareRoot(645.16));
    ASSERT_EQ(0, squareRoot(0.0));
    ASSERT_EQ(1, squareRoot(1.0));
}
 
TEST(SquareRootTest, NegativeNos) {
    ASSERT_EQ(-1.0, squareRoot(-15.0));
    ASSERT_EQ(-1.0, squareRoot(-0.2));
}

*/


TEST(mdcTest, PositiveNumbers) {
    ASSERT_EQ(1, MyMath::mdc(2, 3));
    ASSERT_EQ(1, MyMath::mdc(3, 2));
    ASSERT_EQ(2, MyMath::mdc(4, 6));
    ASSERT_EQ(2, MyMath::mdc(6, 4));
}

TEST(mdcTest, NegativeNumbers) {
    ASSERT_EQ(2, MyMath::mdc(-2, 4));
}

TEST(mdcTest, ArgZero_Exception) {
    EXPECT_THROW(MyMath::mdc(0,1), std::runtime_error);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
