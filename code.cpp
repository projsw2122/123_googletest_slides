#include <math.h>
 
double squareRoot(const double a) {

    // if (a>35.0) return 35.0; // Provoke an error !!!

    double b = sqrt(a);
    if(b != b) { // nan check
        return -1.0;
    }else{
        return sqrt(a);
    }
}

#include <iostream>
#include <stdexcept>
#include <string>


namespace MyMath{
    /// @brief Calculates the greatest common positive divider between 2 non-zero ints.
    /// If any of the args is zero, it throws a run time error IllegalArgument exception.
    /// @param[in] a The first number 
    /// @param[in] b The second number 
    /// @return return is greatest common positive divider (or throws exception)
    int mdc(int a, int b) 
    {
        if (a == 0 || b == 0)
            throw std::runtime_error(std::string("Ill Arg (") + 
                                                 std::to_string(a)+" , " + 
                                                 std::to_string(b) + ")");
        while (b > 0) {
            int aux = a % b;
            a = b;   
            b = aux;
        }
        return a;
    }
}


/* example main */

//#define WANT_MAIN

#ifdef WANT_MAIN
    int main(int argc, char **argv) 
    {
        int x,y;
        std::cout << "Input first number:";
        std::cin >> x;
        std::cout << "Input second number:";
        std::cin >> y;
        std::cout << MyMath::mdc(x,y) << std::endl;
    }
#endif

