# 123_googletest_slides

Google Test Framework C++ example; tested in Windows 10 (PS and MSYS2) and Linux Xubuntu 20.04.
Includes testing an exception.

Very Small Example of C++ Unit Testing with GoogleTest 

"Software Design" course of M.EEC, in FEUP, Portugal

Main contributor: Armando Sousa - mailto:asousa@fe.up.pt


# License
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)
In short, free software: Useful, transparent, no warranty


# Installing Google Test C++ Framwork, building and running the tests

Tested on Windows (power Shell) and Linux

Google Test C++ Framework, Google Test or GTest for short ;)

## Install GTest

Naturally, needs GoogleTest libraries.

On Linux, *adapted* from https://www.eriksmistad.no/getting-started-with-google-test-on-ubuntu/ :
````
sudo apt-get install libgtest-dev
sudo apt-get install cmake # install cmake
cd /usr/src/gtest
sudo cmake CMakeLists.txt
sudo make
# copy or symlink libgtest.a and libgtest_main.a to your /usr/lib folder
sudo cp lib/*.a /usr/lib
````

On windows, with MSYS2 and its pacman manager:
````
pacman -S mingw-w64-x86_64-gtest
````

## Installing CMake on windows

You can build the executable from the command line (see below) but 
if looking to automate build system (cross platform), also needs CMake, on Windows MSYS2:
 * `pacman -S mingw-w64-x86_64-cmake`
 * Optionally: `pacman -S mingw-w64-x86_64-extra-cmake-modules`
 * If CMake complains no make, either install make our ninja: `pacman -S mingw-w64-x86_64-ninja`



## Build this example (with CMAKE)

These procedures are cross platform.

Before first run, delete CMakeCache.txt (if any), then:
````
cmake CMakeLists.txt
````
CMake will produce either a 
    * `build.ninja` file for the `ninja` tool 
  or 
    * a `Makefile` for the `make` tool

...and then run either `make` or `ninja`.

This will produce the runTest.exe executable.


## Build this example with command line with g++

Manual compilation without the tests:
`g++ -Wall -D WANT_MAIN code.cpp -o example.exe`


Manual compilation with the tests:
`g++ -Wall tests.cpp -lgtest -lgtest_main -lpthread -o runTests.exe`


## Run the tests

To run all the tests: `./runTests`

To run all the tests (prettier colors): `./runTests --gtest_color=yes`


# Additional information
 * GoogleTest User’s Guide -  https://google.github.io/googletest/
 * Google Test Wiki - http://code.google.com/p/googletest/wiki/Documentation
 * Google Test Release Git - https://github.com/google/googletest/tree/release-1.10.0
 * IBM A quick introduction to the Google C++ Testing Framework - http://www.ibm.com/developerworks/aix/library/au-googletestingframework.html

 * MSYS2 (compiler and package manager for windows, in order to achieve cross platform) 
    * https://www.msys2.org/wiki/MSYS2-installation/
    * https://packages.msys2.org/package/
    * https://packages.msys2.org/package/mingw-w64-x86_64-gtest
    * https://packages.msys2.org/package/mingw-w64-x86_64-gtest?repo=mingw64

To install Google Test on MSYS2, Windows: ` pacman -S mingw-w64-x86_64-gtest `

Try instructions above. If not working, then try instalations that you might need
  * https://packages.msys2.org/package/mingw-w64-x86_64-cmake?repo=mingw64
    * `pacman -S mingw-w64-x86_64-cmake`
  * https://packages.msys2.org/package/mingw-w64-x86_64-extra-cmake-modules?repo=mingw64
    * `pacman -S mingw-w64-x86_64-extra-cmake-modules`
  * https://packages.msys2.org/package/mingw-w64-x86_64-ninja?repo=mingw64
    * `pacman -S mingw-w64-x86_64-ninja`

  * Note that if you are using CMake and ninja, then instead of `make` you should `ninja`
    * In this mode, CMake produces a `build.ninja` file for the `ninja` tool




## Sample session, Example input and output

````
PS > g++ -Wall tests.cpp -lgtest -lgtest_main -lpthread -o runTests.exe
PS > ./runTests --gtest_color=yes
[==========] Running 3 tests from 1 test suite.
[----------] Global test environment set-up.
[----------] 3 tests from mdcTest
[ RUN      ] mdcTest.PositiveNumbers
[       OK ] mdcTest.PositiveNumbers (0 ms)
[ RUN      ] mdcTest.NegativeNumbers
tests.cpp:32: Failure
Expected equality of these values:
  2
  MyMath::mdc(-2, 4)
    Which is: 4
[  FAILED  ] mdcTest.NegativeNumbers (36 ms)
[ RUN      ] mdcTest.ArgZero_Exception
[       OK ] mdcTest.ArgZero_Exception (6 ms)
[----------] 3 tests from mdcTest (134 ms total)

[----------] Global test environment tear-down
[==========] 3 tests from 1 test suite ran. (194 ms total)
[  PASSED  ] 2 tests.
[  FAILED  ] 1 test, listed below:
[  FAILED  ] mdcTest.NegativeNumbers

 1 FAILED TEST
PS >
````
